from common.json import ModelEncoder
from .blank_models import Contact

class ContactEncoder(ModelEncoder):
    model = Contact
    properties = [
        "id",
        "first_name",
        "last_name",
        "email",
        "message",
    ]
