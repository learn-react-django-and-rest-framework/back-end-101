from django.views.decorators.http import require_http_methods
from .blank_models import Contact
from django.http import JsonResponse
import json
from .blank_encoders import ContactEncoder




@require_http_methods(["GET", "POST"])
def contacts_list(request):



    if request.method == "GET":
        contacts = Contact.objects.all()
        return JsonResponse(
            {"contacts": contacts},
            encoder = ContactEncoder
        )



    else: #POST
        content = json.loads(request.body)
        contact = Contact.objects.create(**content)
        return JsonResponse(
            contact,
            encoder=ContactEncoder,
            safe=False,
        )





@require_http_methods(["DELETE", "GET", "PUT"])
def contact_detail(request, id):



    if request.method == "GET":
        try:
            contact = Contact.objects.get(id=id)
            return JsonResponse(
                contact,
                encoder=ContactEncoder,
                safe=False
            )
        except Contact.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



    elif request.method == "DELETE":
        try:
            contact = Contact.objects.get(id=id)
            contact.delete()
            return JsonResponse(
                contact,
                encoder=ContactEncoder,
                safe=False,
            )
        except Contact.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})



    else: # PUT
        try:
            content = json.loads(request.body)
            Contact.objects.filter(id=id).update(**content)
            contact = Contact.objects.get(id=id)
            return JsonResponse(
                contact,
                encoder=ContactEncoder,
                safe=False,
            )
        except Contact.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

