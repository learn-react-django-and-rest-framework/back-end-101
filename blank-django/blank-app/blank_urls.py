from django.urls import path
from .blank_views import contacts_list, contact_detail

urlpatterns = [
    path("", contacts_list, name="contacts_list"),
    path("<int:id>/", contact_detail, name="contact_detail"),
]
