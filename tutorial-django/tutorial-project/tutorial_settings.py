
ALLOWED_HOSTS = []

INSTALLED_APPS = [
    "corsheaders",
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'YOUR_APP_NAME_HERE.apps.AppNameConfig',
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

CSRF_TRUSTED_ORIGINS = [
    "http://localhost:3000",
]

CORS_ALLOWED_ORIGINS = [
    "http://localhost:3000",
]
CORS_ALLOW_CREDENTIALS = True


# There are a lot of things you should not touch in your Settings.py, but these are the things you should touch.
# There's a SUPER annoying thing called CORS, which basically means that when you go to add React to a project, it'll
# blow up in your face and tell you no. Because it thinks you're a bad guy trying to use our web app.
# Stupid, right? (just kidding, CORS!)

# The most notable difference you will have to make here is on line 12.
# In Django, there are two separate parts of a Django project. The Project, and the App. The Project is where Settings.py is stored, and it needs to connect to
# the app. In the Tutorial's case, it would be written as folows:

    # 'tutorial-project.apps.TutorialAppConfig'

# This is something you'll have to fix by hand every time you initalize a project.



# (If you use the REAL REACT DJANGO PROJECT template I provide in the README.md, this segment will be ENTIRELY done for you.)