# EVERYTHING IN QUOTES, YOU CAN JUST IGNORE!!

"""
URL configuration for portfolio_project project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# SERIOUSLY, YOU CAN JUST DO THE STUFF BELOW!


from django.contrib import admin # import the "admin" page from Django
from django.urls import path, include # import the "path" function and "include" function

urlpatterns = [ # fill in urlpatterns with information
    path('admin/', admin.site.urls), # This comes with your project. DON'T edit it! It just sends you to your admin page.
    path("app/", include("app_name.urls")), # This path will connect the project to the URLs.py in your app.
]


# Well, why is this important? I already have a URLs.py
# HA! You only THINK you have a URLs.py!
# The one you filled in earlier only exists in the App. The Django APP is connected to the Admin page,
# but is NOT connected to this URLs page. Django's project manager only knows of ONE URL page, since the
# other didn't come with it.

# The "include" function will tell Django to include the URL paths in your other page.
# Technically you could do them all here, but that would involve cross-importing and less organization.

# Now, one thing to note:

# Because your path to your urls.py starts with "app/", ALL of your URLs from that file will ALSO start 
# with "app/". That means, if you want to go to the Contact Detail at the ID of one, you would go to 
# "http://localhost:8000/app/1", rather than just a 1 at the end.