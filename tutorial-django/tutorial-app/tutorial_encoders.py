from common.json import ModelEncoder # From the JSON file in the Common folder, import ModelEncoder
from .tutorial_models import Contact # from our tutorial models, import the Contact model we made

class ContactEncoder(ModelEncoder): # Create a class called Contact Encoder, set to the parameters of ModelEncoder.
    model = Contact # Set the model of the encoder to Contact, which we imported.
    properties = [ # Set the properties of the encoder to those created in the models.py.
        "id", # Django automatically creates an ID for every model; you don't have to set a field for this in your models.
        "first_name", # Takes the property of the "First Name" field
        "last_name", # Takes the property of the "Last Name" field
        "email", # Takes the property of the "Email" field
        "message", # Takes the property of the "message" field
    ]




# What is an encoder used for? Good question.
# Essentially, this line of code right here makes it a million times easier to reference the information in a Model.
# It takes the base of the encoder, from common.json, called ModelEncoder.

# YOU DO NOT NEED TO EDIT THE ModelEncoder!!!
# It is FINE how it is, and I will not be adding notes on it. Please, just trust me on this. It is a base for any kind
# of encoder you want, feel free to just steal the entire json.py file for whatever you want to use it on.
# Seriously, it's so much effort to explain every detail happening in there, so let's just let it be...


# IMPORTANT! If you are using a FOREIGN KEY field in your model, you will need ANOTHER encoder for that model it is referencing!
# (If you don't know what I'm talking about, you don't need to!)
# You can reference the other encoder by doing the following at the end. In this example, the "last_name" field is a Foreign Key.

#     encoders = {
#         "last_name": LastNameEncoder
#     }

# LastNameEncoder, in this example, would have to be ABOVE the other encoder.