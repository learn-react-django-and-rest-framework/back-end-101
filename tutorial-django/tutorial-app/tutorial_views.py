from django.views.decorators.http import require_http_methods #import a decorator that labels HTTP methods
from .tutorial_models import Contact # Import our model, Contact
from django.http import JsonResponse # Import JsonResponse from Django's Base
import json # Import the JSON module for free manipulation of information
from .tutorial_encoders import ContactEncoder #Import the Encoder

# A brief lesson before we start:
# HTTP requests are very important to understand. They are instructions for a 
# browser to use. There are four main ones used here.

# 1. GET 
#     GET is used to retrieve information from a database. When you load
#     a webpage, it GETs the information from the creator's server, then
#     puts it on your page.
# 2. POST
#     POST is used to create new information in a database. Just like a
#     POST on the-artist-formerly-known-as Twitter, it creates something
#     that other people can reTweet, Like, Share, or Report.
# 3. DELETE
#     I, uh... I think you can figure out what a DELETE method does.
# 4. PUT
#     If you use a PUT method, you are grabbing information from the website,
#     and PUTTING it back in as something different. It's basically editing
#     information.

# Proceed as usual.



@require_http_methods(["GET", "POST"]) #The only HTTP Methods that this function can use are GET and POSt
def contacts_list(request): #Create a new function called Contacts_List, with the parameter Request.



    if request.method == "GET": #If you are asking to retrieve information in a list form,
        contacts = Contact.objects.all() #set a variable to ALL objects stored in the Contact model
        return JsonResponse( # Return a JSON Formatted version of the information:
            {"contacts": contacts}, # Creates a dictionary of the information,
            encoder = ContactEncoder # Organizes it using our encoder
        )



    else: # Because we are only using TWO methods, if it isn't GET, it has to be POST
        content = json.loads(request.body) # set the variable "content" equal to a JSON formatted string
        contact = Contact.objects.create(**content) # A variable that creates a new object from the content variable
        return JsonResponse( # Return a JSON formatted version of the information:
            contact, # Return the new Contact information
            encoder=ContactEncoder, # Organize it using our encoder
            safe=False, # Allows JSON to accept any Python info type (don't worry about it)
        )





@require_http_methods(["DELETE", "GET", "PUT"]) # Allows HTTP requests of DELETE, GET, and PUT
def contact_detail(request, id): # Create a function that has params for request, and id



    if request.method == "GET": # If the user is trying to get ONE object's information
        try: # Keep attempting to make this work...
            contact = Contact.objects.get(id=id) #Find a contact that has a matching ID to the one provided
            return JsonResponse( # Return a JSON formatted version of the info:
                contact, # return the singular Contact info
                encoder=ContactEncoder, # Use our Contact Encoder
                safe=False # allow JSON to accept any type of Python info type (again)
            )
        except Contact.DoesNotExist: # However, if the GET method finds nothing,
            response = JsonResponse({"message": "Does not exist"}) #Return a JSON message saying it isn't there
            response.status_code = 404 #Set the error code to 404 (you know that one!)
            return response # Return the entire response with the error code included.



    elif request.method == "DELETE": # If the HTTP request isn't a GET, but it IS a DELETE,
        try: #Attempt to make this work...
            contact = Contact.objects.get(id=id) #Find a contact model by its ID
            contact.delete() # Straight up just delete it.
            return JsonResponse( # For those of you using back-end technologies, return JSON,
                contact, # including the contact you just deleted,
                encoder=ContactEncoder, # formatted using our encoder,
                safe=False, # and accepting any python info type
            )
        except Contact.DoesNotExist: #However, if the DELETE method can't find anything at that ID,
            return JsonResponse({"message": "Does not exist"}) #Return the message that it can't find it



    else: # The only leftover method is PUT, so we just do a blanket term of "else"
        try: #Attempt to make this work...
            content = json.loads(request.body) #Convert the information into a string using JSON
            Contact.objects.filter(id=id).update(**content) #Find the contact by ID, then update with the NEW content
            contact = Contact.objects.get(id=id) # Set contact to the NEW object found by ID
            return JsonResponse( #
                contact, # return the singular Contact info
                encoder=ContactEncoder, # Use our Contact Encoder
                safe=False # allow JSON to accept any type of Python info type (again)
            )
        except Contact.DoesNotExist: # However, if the PUT method finds nothing matching that ID,
            response = JsonResponse({"message": "Does not exist"}) #Return a JSON message saying it isn't there
            response.status_code = 404 #Set the error code to 404,
            return response # Return the entire response with the error code included.


# Here's a brief analogy to help with your understanding of everything you've done 
# so far:

# Models.py is the STRUCTURE of a house. It is the walls, floor, and ceiling.
# Encoders.py is the INTERIOR of a house. It is the furniture, the paint, the rug.
# Views.py is the PEOPLE of a house. It is the Mom, the Dad, and the Kid.

# You can have Views that don't live in a Model, but they're gonna have a hard time.
# Every Model needs their own set of Views.
# And, well, Views living in a Model without any Encoders is going to be boring and ugly.

# For the most part, unless you want to allow a user to update information or delete it themselves,
# you usually don't need the PUT or DELETE method. That's just for high-level messaging or posting type stuff.
# At that point, just get rid of lines 76 to 104. That should be more than enough utilization.

# Make sure the Views you are creating here line up to the URLs you'll look at next!