from django.urls import path #From Django's URL module, import the function Path
from .tutorial_views import contacts_list, contact_detail #from our Views.py, import the functions!

urlpatterns = [ # Set the variable urlpatterns, as referenced in Django, to a list of paths:
    path("", contacts_list, name="contacts_list"), # create a blank path that does whatever Contacts_List does
    path("<int:id>/", contact_detail, name="contact_detail"), # create a path to an item's id that does Contact_Detail
]



# This URLs.py does NOT come with Django automatically.

# I REAPEAT, FOR THOSE IN THE BACK: YOU HAVE TO MAKE THIS FILE YOURSELF!
# After making this file and filling it in with all of the URLs to your heart's content,
# you MUST reference it in the PROJECT'S URLs.py.


# urlpatterns is used a lot in Django's behind-the-scenes area. It basically tells
# the Django framework where to store the information, and where you can do 
# specific things with it.

# For example, in the blank path:
#     1. You are creating a "blank path", meaning by default, it's just a "/" on Google.
#     2. This "blank path" is an entire list of all the items that contacts_list references.

# In the Path underneath it...:
#     1. It is set to <int:id> - this means it will accept any integer. The integer,
#         however, has to be equal to an ID that Django assigns your information.
#     2. It is given the contact_detail view, meaning it just looks at ONE item
#         in your list.

# You MUST complete Views.py before you can get here.