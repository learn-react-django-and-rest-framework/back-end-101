from django.contrib import admin #From Django's settings, import the admin page.
from .tutorial_models import Contact # From our Models (the base of our data), import our model Contact

# Register your models here.
@admin.register(Contact) #Tell Django to register the model "Contact" in our admin page.
class ContactAdmin(admin.ModelAdmin): #Create a class called ContactAdmin, set as an admin.ModelAdmin
    list_display = [ #This lets Django know that you want to show a list of all of the Model's details.
        "first_name", # This is the "First Name" that you can input in Django Admin
        "last_name", # This is the "Last Name" that you can input in Django Admin
        "email", # This is the "Email" that you can input in Django Admin. It MUST be a valid Email address format.
        "message" # This is the "Message" that you can input in Django Admin.
    ]
