from django.db import models # From Django's Database, import models



class Contact(models.Model): # Create a class called Contact, which is set to a model
    first_name = models.CharField(max_length=100) # Create a field called first_name as a CharField
    last_name = models.CharField(max_length=100) # Create a field called last_name as a CharField
    email = models.EmailField() # Create a field called email set to an EmailField
    message = models.TextField() # Create a field called message set to TextField

    def __str__(self): # If a string version of this class is called,
        return self.first_name # Return the "first_name" field as the title
    

# Okay, so let's talk about Models. They are literally the bread and butter of a Django
# project. When we declare them, we're creating new information for the Database that
# Django is equipped with. Database? Huh? Yeah, let's not open that can of worms.
# Your database is basically like a Memory Card for your GameCube. It holds the info.

# The fields (or the lines after the line that starts with "class") are the lines of 
# information that you can give the model "Contact". Looking at the set of fields,
# you'll notice that "first_name" and "last_name" have a thingy called "max_length=100"
# attached to the end. A CharField is basically a small bubble of text that a user can
# fill out, but it's max amount is 250 characters or so. Because of this, you HAVE
# to specify how many characters can fit in there!

# If you need text that is longer than 250-ish characters, use a TextField. You don't
# have to specify how long it will be, and it does the same thing - like in the message
# field.

# As you can assume, the Email field has to be a valid email, because it's something
# built into Django.

# Now, what the heck is that thing at the bottom?

# your def __str__(self) function is so, so important. Basically, it tells Django that
# places like the Admin Page want to reference your ENTIRE model, it can call it something.
# If you try to call an entire model as the name, it's not going to do it. It can't make
# one big item out of all this info. So, if you set that line up, in your Admin Page,
# it won't refer to it as some random thing.
# It will make a list full of your "first_name" items. (trust me on this)
# You can change "first_name" on that function to be whatever field you want, like if you
# want to reference people by their emails, just change it to "email"!

