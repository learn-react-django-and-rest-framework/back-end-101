Welcome to Back End 101!

This is STRICTLY a tutorial for editing and familiarizing yourself with the file types.
After completing this, if you feel ready, follow the link at the bottom labeled "REAL REACT DJANGO PROJECT" and work from there.


In these documents, I have created several kinds of pages you will need to navigate through Django, as well as a few additional pages that spice up a project.
There is NO front-end in this project! I suggest you use ReactJS as your front-end framework, but I will soon be releasing another tutorial on how to use ReactJS, if you DO need help in that department. Aside from that, there are two major segments to this project: one folder of Tutorial Files, which are all annotated, and a folder of Blank Files, which will help you get started if you learn best without that kind of notes. I suggest you start with the tutorial files.


THESE INSTRUCTIONS ARE ONLY FOR WINDOWS OS USERS! (I'm poor and don't have a Mac)



**TO SET UP**

Please ensure you have "Git" installed in your computer, from this link: https://gitforwindows.org/

In the GitLab project, search for an option in the Top Right called *Forks*. Click this.
Title the project whatever you'd like, then set your *Project URLs* namespace to **your username** from the dropdown. (This ensures it will be public!)
Click Fork Project at the bottom when you're ready.
Then, find the blue *Clone* button, and select it. Copy the link titled *Clone with HTTPS*, and keep it for the next step.

Open a *Terminal*. If you search your apps, you'll find one called "terminal"... There is a terminal that comes with your VSCode, but we don't suggest using that to start.

*Windows Terminal:*

    1. Open the terminal app. It looks intimidating, but it won't be after long.
    2. THIS STEP VARIES! TRY a. FIRST, THEN TRY b. IF YOU RECEIVE AN ERROR!
        a. type "cd desktop"
        b. type "cd onedrive" THEN "cd desktop"

Once you're in the right spot, type "git clone", then paste your URL you copied from the GitLab project, and click enter.
Congrats! You now have a version of this project that ONLY YOU can see on your device!

In your *Terminal*, type the following:

    cd <put the name of your project here WITHOUT spaces (try - or _)>

PIP will likely need to update, but feel free to ignore it - the rest of us do, too.
Once PIP has finished installing all your requirements, type "code ." - DON'T forget the period! It tells the computer to open SPECIFICALLY the folder you've selected in your code editor.

And there you go! You got the project ready to edit! Remember, these are good for reference, but won't actually create a project for you.



**TO USE**

In the folders attached, there's one called Tutorial Files, and one called Blank Files. Start with the Tutorial Files if you have questions about what needs to go into a React project. If you just saved this for the template, great! Pop into the Blank Files and copy away.


In Tutorial Django, you will find:

    1. common
        a. In common, you will find something called json.py - I said it once, twice, thrice, I'll say it until I die. DO NOT edit this code! You'll only make things harder for yourself!
    2. tutorial-app
        a. You will find five files, all ending in ".py" - these are the base of a Django project, written in Python! We are using a REST API structure for our project, which if you plan on sharing with others, will make you sound like you know what you're doing! :D
        b. These files are all annotated to high heaven, and hopefully will help you figure out what to do with them.
    3. tutorial-project
        a. You'll find two files, both ending in ".py" as well. These are for a project's base setup, and do not change from project to project besides a few naming conventions.
        b. These files are less annotated, as you don't usually do much with them.

    In all of these files, please note that these are NOT the only files that come with a Django project - they're just the only ones that matter.

In Blank Django, you will find:

    1. A common folder, a blank-app folder, and a blank-project folder.
    2. Seven project files, titled similarly to the tutorial files.
    3. NO notes. This one is all you! They are also helpful resources for coding real projects in Django!

Hop into the Tutorial Files and start poking around.

If you made something you really like and want to save it for later online...
Type the following lines, one by one, into your terminal:

    git add .
    git commit -m "Type here what you did with the project!"
    git push origin main

When it's finished loading, check out GitLab and see if it worked. Good job!


**WHEN YOU ARE DONE**

Congrats, you may (or may not) have a better understanding of React now! Our next step suggestions are:

    1. Check out our Front End 101 project, linked at the bottom, to learn what goes into a ReactJS application.
    2. If you've already done that, go to the link titled "REAL REACT DJANGO PROJECT," and give your skills a shot.

Best of luck, baby coder!



LINK TO: Front-End-101
https://gitlab.com/learn-react-django-and-rest-framework/front-end-101

LINK TO: REAL REACT DJANGO PROJECT
https://gitlab.com/learn-react-django-and-rest-framework/real-react-django-project